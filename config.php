<?php
/**
 * B A S I C  C O N F I G
 */
$cfg['basePath']          = __DIR__;
$cfg['indexFile']         = ''; //index.php lub ew. empty string jeśli mamy wpis w .htaccess
$cfg['dirControllers']    = 'application/controller';
$cfg['dirModel']          = 'application/model';
$cfg['dirView']           = 'application/view';
$cfg['defaultController'] = 'home';
$cfg['defaultMethod']     = 'index';



/*
 * D A T A B A S E
 * 
 * Driver: PostgreSQL
 * host: FQDN/IP
 * port: int
 * user: str
 * password: str
 * dbname: str
 * 
 * -------------------------------------------------------------
 * 
 * Driver: SQLite3
 * dbname: ścieżka do pliku np: application/db/mydb.sqlite3
 * host: bool - czy utworzyć plik bazy jeśli nie istnieje.
 * 
 * -------------------------------------------------------------
 * 
 * Driver: ''
 * Zostanie użyty sterownik nullDb(pusta zaślepka)
 * 
 * 
 * Dla tego projektu z powodzeniem i bez konsekwencji można wyłączyć bazę danych(jeśli nie chcemy logować zapytań)
 */
$cfg['database'] = [
  'driver'   => 'SQLite3',
  'host'     => TRUE,
  'port'     => '',
  'user'     => '',
  'password' => '',
  'dbname'   => 'resources/sqlite/formatter.db'
];


/**
 * R O U T E S
 */
$cfg['routes'] = [
  'format.html' => 'home/format',
];

