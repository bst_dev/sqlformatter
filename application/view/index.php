<!DOCTYPE html>
<html lang="PL">
  <head>
    <title>SQL Formatter</title>
    <meta charset="utf-8">
    <meta name="description" content="}">
    <meta name="author" content="BaSta">
    <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no, minimal-ui" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="msapplication-tap-highlight" content="no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="<?PHP echo siteUrl('resources/css/main.css') ?>" rel="stylesheet">
  </head>
  <body>

    <div id="sqlFrom">
      <form action="<?PHP echo siteUrl('format.html'); ?>" method="POST">
        <textarea name="sql" id=""><?PHP echo isset($sql) ? $sql : null ?></textarea>
        <input type="submit" value="Format" />
      </form>
    </div>

    <?PHP if(isset($sqlFormatted)): ?>
      <div class="output">
        <h1>Formatted:</h1>
        <?PHP echo $sqlFormatted ?>
      </div>
    <?PHP endif ?>

    <?PHP if(isset($sqlCompressed)): ?>
      <div class="output">
        <h1>Compressed:</h1>
        <?PHP echo $sqlCompressed ?>
      </div>
    <?PHP endif ?>


  </body>

  <script>
    var textarea = document.getElementById("textarea");
    var heightLimit = 800;

    textarea.oninput = function () {
      textarea.style.height = "";
      textarea.style.height = Math.min(textarea.scrollHeight, heightLimit) + "px";
    };
  </script>

</html>
