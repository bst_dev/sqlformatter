<?php

namespace application\controller;

use lib\external\SqlFormatter as SqlFormatter;

/**
 *
 * @author BaSta
 * @property \application\model\devicesModel $devicesModel 
 * @property \application\model\formatterModel $formatterModel
 */
class home extends \application\core\baseController {

  private $devicesModel;
  private $formatterModel;





  public function __construct() {
    parent::__construct();
    $this->formatterModel = new \application\model\formatterModel();
  }



  /**
   * Lista serwerów
   */
  public function index() {

    $this->view->renderView(
      'index', []
    );
  }



  public function format() {
    $sql = trim($this->input->post('sql'));

    if(!$sql)
      return $this->index();

    $this->formatterModel->addQuery($sql);

    $sqlFormatted  = SqlFormatter::highlight(SqlFormatter::format($sql, false));
    $sqlCompressed = SqlFormatter::compress($sql);

    $this->view->renderView(
      'index', [
      'sql'           => $sql,
      'sqlFormatted'  => $sqlFormatted,
      'sqlCompressed' => $sqlCompressed
      ]
    );
  }



}
