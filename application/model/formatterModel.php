<?php

namespace application\model;

/**
 *
 * @author BaSta
 */
class formatterModel extends \lib\mvc\m\model {

  public function __construct() {
    parent::__construct();
    $this->createTablesIfNotExists();
  }



  private function createTablesIfNotExists() {

    if($this->isDbNotConnected())
      return;

    $this->db->query('
      CREATE TABLE IF NOT EXISTS queries(
        id INTEGER PRIMARY KEY AUTOINCREMENT, 
        query TEXT NOT NULL, 
        public BOOLEAN NOT NULL, 
        hash TEXT UNIQUE NOT NULL
      )
    ');
  }



  public function addQuery($query) {

    if($this->isDbNotConnected())
      return;

    $data = bin2hex($query);
    $hash = sha1(microtime().uniqid('sqf'));

    $this->db->query("
      INSERT INTO queries(query, public, hash) 
      VALUES('$data', 'FALSE', '$hash') 
    ");

    return $hash;
  }



}
