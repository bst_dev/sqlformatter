<?php

namespace application\core;

/**
 *
 * @author BaSta
 */
class baseController extends \lib\mvc\c\controller {

  protected $viewValuesHeader  = [];
  protected $viewValuesTopMenu = [];
  protected $viewValuesFooter  = [];
  protected $tplVars           = [];
  protected $seo               = [];
  protected $breadcrumbs       = [];





  public function __construct() {
    parent::__construct();
  }



  /**
   * Ustawia wartość dla template
   * @param str $key
   * @param mixed $value
   * @return \application\core\collectiveController
   */
  protected function setTplVar($key, $value) {
    $this->tplVars[$key] = $value;
    return $this;
  }



}
