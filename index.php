<?php
error_reporting(E_ALL);

ini_set("display_errors", 1);
include 'lib/autoload.php';

ini_set('xdebug.var_display_max_depth', 10);
ini_set('xdebug.var_display_max_children', 512);
ini_set('xdebug.var_display_max_data', 2048);

use lib\mvc\base\app as app;
use lib\mvc\base\router as router;

include 'config.php';


app::$basePath          = $cfg['basePath'];
app::$indexFile         = $cfg['indexFile'];
app::$dirControllers    = $cfg['dirControllers'];
app::$dirModel          = $cfg['dirModel'];
app::$dirView           = $cfg['dirView'];
app::$defaultController = $cfg['defaultController'];
app::$defaultMethod     = $cfg['defaultMethod'];

app::$databaseDriver   = $cfg['database']['driver'];
app::$databaseHost     = $cfg['database']['host'];
app::$databasePort     = $cfg['database']['port'];
app::$databaseUser     = $cfg['database']['user'];
app::$databasePassword = $cfg['database']['password'];
app::$databaseName     = $cfg['database']['dbname'];


router::$routes = $cfg['routes'];


global $app;
$app = app::getInstance()->runForestRun();

