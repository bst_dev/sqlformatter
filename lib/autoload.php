<?php
spl_autoload_register(function ($class_name){


  $path     = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
  $filePath = preg_replace('%/lib$%ix', '', __DIR__).DIRECTORY_SEPARATOR.$path.'.php';

  require_once $filePath;
});
