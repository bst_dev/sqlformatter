<?php

namespace lib\mvc\m;

/**
 *
 * @author BaSta
 * @property \lib\mvc\base\lib\database\dbBase $db 
 */
abstract class model {

  protected $cache = [];
  protected $db    = null;





  public function __construct() {
    $driver   = \lib\mvc\base\app::$databaseDriver;
    $host     = \lib\mvc\base\app::$databaseHost;
    $name     = \lib\mvc\base\app::$databaseName;
    $password = \lib\mvc\base\app::$databasePassword;
    $port     = \lib\mvc\base\app::$databasePort;
    $user     = \lib\mvc\base\app::$databaseUser;

    if(!$driver)
      $driver = 'nullDb';

    $dbDrvClassName = "\\lib\mvc\\base\\lib\\database\\$driver";
    $this->db       = new $dbDrvClassName([
      'host'     => $host,
      'port'     => $port,
      'user'     => $user,
      'password' => $password,
      'database' => $name,
    ]);
  }



  /**
   * Zwraca dane z cache lub z przekazanej funkcji
   * @param callable $callback
   * @param str|array $key unikalny klucz dla cache. Jeśli array - to klucz będzie stworzony przez implode z separatorem '-'
   * @return mixed
   */
  protected function getCachedData(callable $callback, $key) {

    if(is_array($key))
      $key = implode('-', $key);

    if(isset($this->cache[$key])){
      return $this->cache[$key];
    }

    $this->cache[$key] = $callback();

    return $this->cache[$key];
  }



  protected function isDbNotConnected() {
    if($this->db instanceof \lib\mvc\base\lib\database\nullDb){
      return true;
    }

    return false;
  }



}
