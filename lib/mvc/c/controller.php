<?php

namespace lib\mvc\c;

/**
 *
 * @author BaSta
 * @property \lib\mvc\base\lib\input $input
 * @property \lib\mvc\v\view $view
 * @property \lib\mvc\base\router $router 
 */
abstract class controller {

  public $input;
  public $view;
  public $router;





  public function __construct() {
    $this->input  = new \lib\mvc\base\lib\input();
    $this->view   = new \lib\mvc\v\view();
    $this->router = \lib\mvc\base\app::getInstance()->router;
  }



}
