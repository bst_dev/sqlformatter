<?php

namespace lib\mvc\v;

use lib\mvc\base\app as app;

/**
 *
 * @author BaSta
 */
class view {

  public $siteUrl;





  /**
   * @return \lib\mvc\v\view
   */
  public function __construct() {

    $this->siteUrl = app::getInstance()->router->baseUrl;

    return $this;
  }



  public function renderView($template, array $values = [], $return = false) {

    $filePath = sprintf('%s/%s/%s.%s', app::$basePath, app::$dirView, $template, 'php');

    if(!file_exists($filePath))
      die('view: '.$template.' does not exists in: '.$filePath);


    if(!empty($values))
      extract($values);


    if($return){
      ob_start();
      include $filePath;
      return ob_get_clean();
    }

    include $filePath;
  }



}
