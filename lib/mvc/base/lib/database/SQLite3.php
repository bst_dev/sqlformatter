<?php

namespace lib\mvc\base\lib\database;

/**
 *
 * @author BaSta
 * @property \SQLite3 $db
 */
class SQLite3 extends dbBase {

  protected function connect(array $cfg) {

    $dbFile = \lib\mvc\base\app::$basePath.'/'.$cfg['database'];

    if(!file_exists($dbFile)){
      if($cfg['host'] === TRUE){
        $this->mkDbFile($dbFile);
      }else{
        die($dbFile.' does not exists');
      }
    }

    $this->db = new \SQLite3($dbFile);
  }



  private function mkDbFile($filePath) {
    $path = dirname($filePath);

    if(!file_exists($path))
      mkdir(dirname($filePath), 0777, true);

    touch($filePath);
    chmod($filePath, 0664);
  }



  public function bool2bool($str) {
    
  }



  public function query($query) {
    $this->db->query($query);

    return $this;
  }



  public function resultArray() {
    
  }



  public function rowArray() {
    
  }



  public function select($str) {
    
  }



}
