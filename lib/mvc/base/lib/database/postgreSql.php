<?php

namespace lib\mvc\base\lib\database;

/**
 *
 * @author BaSta
 */
class postgreSql extends \lib\mvc\base\lib\database\dbBase {

  private $res;





  protected function connect(array $cfg) {
    $connectionStr = sprintf(
      'host=%s port=%d dbname=%s user=%s password=%s', $cfg['host'], $cfg['port'], $cfg['database'], $cfg['user'], $cfg['password']
    );

    $this->db = pg_connect($connectionStr);
  }



  /**
   * 
   * @param type $query
   * @return \lib\mvc\c\database\postgreSql
   */
  public function query($query) {
    $this->res = pg_query($this->db, $query);
    return $this;
  }



  public function resultArray() {
    $data = pg_fetch_all($this->res);
    return $data;
  }



  public function rowArray() {
    $data = pg_fetch_array($this->res, NULL, PGSQL_ASSOC);
    return $data;
  }



  public function bool2bool($str) {
    return $str == 't' ? true : false;
  }



  public function insert() {
    
  }



  public function where() {
    
  }



  public function join() {
    
  }



  public function select($str) {
    
  }



  public function like() {
    
  }



  public function ilike() {
    
  }



}
