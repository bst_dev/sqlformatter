<?php

namespace lib\mvc\base\lib\database;

/**
 *
 * @author BaSta
 */
abstract class dbBase {

  protected $db;





  public function __construct(array $cfg) {
    $this->connect($cfg);
  }



  abstract protected function connect(array $cfg);

  /**
   * @return dbBase
   */
  abstract public function query($query);

  abstract public function resultArray();

  abstract public function rowArray();

  /**
   * Funckcja konwertująca bool z db na bool php (np. dla postgreSQL który zwraca t/f )
   */
  abstract public function bool2bool($str);

  abstract public function select($str);
}
