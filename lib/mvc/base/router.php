<?php

namespace lib\mvc\base;

/**
 *
 * @author BaSta
 */
class router {

  public $host;
  public $url;
  public $uri;
  public $protocol;
  public $controller;
  public $method;
  public $arguments;
  public $uriRouted;
  public $baseUrl;
  public static $indexFile;
  public static $defaultMethod;
  public static $defaultController;
  public static $routes = [];





  public function __construct() {
    $this->host      = $this->getHost();
    $this->protocol  = $this->getProtocol();
    $this->url       = $this->getUrl();
    $this->uri       = $this->getUri();
    $this->init();
    $this->uriRouted = $this->getUriRouted();
    if($this->uriRouted)
      $this->init();
  }



  private function init() {
    $this->controller = $this->getControllerName();
    $this->method     = $this->getMethodName();
    $this->arguments  = $this->getArguments();
    $this->baseUrl    = $this->getSiteUrl();
  }



  public function getUriRouted() {

    $regs = [];
    foreach(array_keys(self::$routes) as $rule){
      if(preg_match('%/'.$rule.'%', $this->getUri(), $regs)){
        break;
      }
    }

    if(empty($regs))
      return false;

    if(is_callable(self::$routes[$rule])){
      unset($regs[0]); //ciekawostka: bardziej efektywna jest próba usunięcią nieistniejącego elementu niż najpierw sprawdzenie czy istnieje.
      call_user_func_array(self::$routes[$rule], $regs);
      die();
    }else{
      $i         = 0;
      $uriRouted = preg_replace_callback('/\$(\d+)/', function($groups) use($regs, $i){
        $i++;
        if(isset($regs[$groups[$i]])){
          return $regs[$groups[$i]];
        }
      }, self::$routes[$rule]);
    }


    return '/'.$uriRouted;
  }



  public function getIndexFile() {
    return self::$indexFile;
  }



  private function getProtocol() {
    return !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https' : 'http';
  }



  private function getHost() {
    return $_SERVER['HTTP_HOST'];
  }



  private function getUrl() {
    return $this->getProtocol().':/'.$_SERVER['REQUEST_URI'];
  }



  private function getUri() {
    if($this->uriRouted)
      return $this->uriRouted;

    return isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : null;
  }



  private function getUriSegments() {
    return preg_split('%/%i', $this->getUri());
  }



  private function getControllerName() {
    $segments = $this->getUriSegments();
    return isset($segments[1]) && !empty($segments[1]) ? $segments[1] : self::$defaultController;
  }



  private function getMethodName() {
    $segments = $this->getUriSegments();
    return isset($segments[2]) && !empty($segments[2]) ? $segments[2] : self::$defaultMethod;
  }



  private function getArguments() {
    $segments = $this->getUriSegments();
    unset($segments[0], $segments[1], $segments[2]);
    return $segments;
  }



  private function getSiteUrl() {
    return sprintf('%s://%s', $this->getProtocol(), $this->getHost());
  }



}
