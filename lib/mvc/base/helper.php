<?php

use lib\mvc\base\app as app;

function siteUrl($segments = '') {
  $file    = app::$indexFile;
  $baseUrl = app::getInstance()->router->baseUrl;


  if($file){
    if($segments){
      $url = sprintf('%s/%s/%s', $baseUrl, $file, $segments);
    }else{
      $url = sprintf('%s/%s', $baseUrl, $file);
    }
  }else{
    if($segments){
      $url = sprintf('%s/%s', $baseUrl, $segments);
    }else{
      $url = sprintf('%s/', $baseUrl);
    }
  }

  return $url;
}



function redirect($url) {
  header("Location: $url");
  die();
}


