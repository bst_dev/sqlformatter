<?php

namespace lib\mvc\base;

use \lib\mvc\base\router as router;

/**
 *
 * @author BaSta
 * @property \lib\mvc\base\app $instance 
 * @property \lib\mvc\base\router $router 
 */
class app {

  private static $instance;
  public static $basePath          = null;
  public static $dirControllers    = 'controller';
  public static $dirModel          = 'model';
  public static $dirView           = 'view';
  public static $indexFile         = 'index.php';
  public static $defaultMethod     = 'index';
  public static $defaultController = 'home';
  public static $databaseDriver    = '';
  public static $databaseHost      = '';
  public static $databaseUser      = '';
  public static $databasePassword  = '';
  public static $databasePort      = '';
  public static $databaseName      = '';
  public $router;





  /**
   * @return \lib\mvc\base\app
   */
  public static function getInstance() {
    if(self::$instance instanceof \lib\mvc\base\app)
      return self::$instance;

    self::$instance = new \lib\mvc\base\app();
    self::init();

    return self::$instance;
  }



  private static function init() {
    if(!self::$basePath)
      self::$basePath = preg_replace('%/lib/mvc/base$%i', null, __DIR__);

    self::loadHelpers();

    router::$indexFile         = self::$indexFile;
    router::$defaultController = self::$defaultController;
    router::$defaultMethod     = self::$defaultMethod;
  }



  private static function loadHelpers() {
    require_once __DIR__.'/helper.php';
  }



  private function getControllerClass() {
    return sprintf(
      '%s\\%s', str_replace('/', '\\', self::$dirControllers), $this->router->controller
    );
  }



  private function getControllerPath() {
    return sprintf(
      '%s/%s', self::$basePath, str_replace('\\', '/', $this->getControllerClass().'.php')
    );
  }



  public function createController() {
    if(!file_exists($this->getControllerPath()))
      die('controller: '.$this->router->controller.' does not exists');

    $class = $this->getControllerClass();

    return new $class();
  }



  public function callControllerMethod(\lib\mvc\c\controller $controller, $method, array $arguments = []) {
    if(!method_exists($controller, $method))
      die('method: '.$method.' does not exists');

    return call_user_func_array(
      [
        $controller,
        $method
      ], 
      $arguments
    );
  }



  /**
   * Uruchamiamy aplikację.
   */
  public function runForestRun() {
    $this->router = new \lib\mvc\base\router();

    $controller = $this->createController();

    $this->callControllerMethod(
      $controller, 
      $this->router->method, 
      $this->router->arguments
    );
  }



}
